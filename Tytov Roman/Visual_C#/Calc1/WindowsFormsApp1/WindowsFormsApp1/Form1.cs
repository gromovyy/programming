﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Plus_Click(object sender, EventArgs e)
        {
            Sign.Text = Plus.Text;
        }

        private void Multiply_Click(object sender, EventArgs e)
        {
            Sign.Text = Multiply.Text;
        }

        private void Minus_Click(object sender, EventArgs e)
        {
            Sign.Text = Minus.Text;
        }

        private void Divide_Click(object sender, EventArgs e)
        {
            Sign.Text = Divide.Text;
        }

        private void Equel_Click(object sender, EventArgs e)
        {
            double num1, num2, ans;
            string a = textBox1.Text, b =textBox2.Text;
            switch (Sign.Text)
            {
                case ("+"):
                    double.TryParse(a,out num1);
                    double.TryParse(b, out num2);
                    ans = num1 + num2;
                    answe.Text = ans.ToString();
                    break;
                case ("-"):
                    double.TryParse(a, out num1);
                    double.TryParse(b, out num2);
                    ans = num1 - num2;
                    answe.Text = ans.ToString();
                    break;
                case ("*"):
                    double.TryParse(a, out num1);
                    double.TryParse(b, out num2);
                    ans = num1 * num2;
                    answe.Text = ans.ToString();
                    break;
                case ("/"):
                    double.TryParse(a, out num1);
                    double.TryParse(b, out num2);
                    ans = num1 / num2;
                    answe.Text = ans.ToString();
                    break;
            }
        }
    }
}
