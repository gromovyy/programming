﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Plus = new System.Windows.Forms.Button();
            this.Equel = new System.Windows.Forms.Button();
            this.Divide = new System.Windows.Forms.Button();
            this.Minus = new System.Windows.Forms.Button();
            this.Multiply = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.Sign = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Answer = new System.Windows.Forms.Label();
            this.answe = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Plus
            // 
            this.Plus.Location = new System.Drawing.Point(12, 143);
            this.Plus.Name = "Plus";
            this.Plus.Size = new System.Drawing.Size(49, 38);
            this.Plus.TabIndex = 0;
            this.Plus.Text = "+";
            this.Plus.UseVisualStyleBackColor = true;
            this.Plus.Click += new System.EventHandler(this.Plus_Click);
            // 
            // Equel
            // 
            this.Equel.Location = new System.Drawing.Point(98, 187);
            this.Equel.Name = "Equel";
            this.Equel.Size = new System.Drawing.Size(49, 38);
            this.Equel.TabIndex = 1;
            this.Equel.Text = "=";
            this.Equel.UseVisualStyleBackColor = true;
            this.Equel.Click += new System.EventHandler(this.Equel_Click);
            // 
            // Divide
            // 
            this.Divide.Location = new System.Drawing.Point(177, 143);
            this.Divide.Name = "Divide";
            this.Divide.Size = new System.Drawing.Size(49, 38);
            this.Divide.TabIndex = 2;
            this.Divide.Text = "/";
            this.Divide.UseVisualStyleBackColor = true;
            this.Divide.Click += new System.EventHandler(this.Divide_Click);
            // 
            // Minus
            // 
            this.Minus.Location = new System.Drawing.Point(122, 143);
            this.Minus.Name = "Minus";
            this.Minus.Size = new System.Drawing.Size(49, 38);
            this.Minus.TabIndex = 3;
            this.Minus.Text = "-";
            this.Minus.UseVisualStyleBackColor = true;
            this.Minus.Click += new System.EventHandler(this.Minus_Click);
            // 
            // Multiply
            // 
            this.Multiply.Location = new System.Drawing.Point(67, 143);
            this.Multiply.Name = "Multiply";
            this.Multiply.Size = new System.Drawing.Size(49, 38);
            this.Multiply.TabIndex = 4;
            this.Multiply.Text = "*";
            this.Multiply.UseVisualStyleBackColor = true;
            this.Multiply.Click += new System.EventHandler(this.Multiply_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(138, 28);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(88, 20);
            this.textBox2.TabIndex = 6;
            // 
            // Sign
            // 
            this.Sign.AutoSize = true;
            this.Sign.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Sign.Location = new System.Drawing.Point(116, 28);
            this.Sign.Name = "Sign";
            this.Sign.Size = new System.Drawing.Size(0, 17);
            this.Sign.TabIndex = 7;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 27);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(88, 20);
            this.textBox1.TabIndex = 8;
            // 
            // Answer
            // 
            this.Answer.AutoSize = true;
            this.Answer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Answer.Location = new System.Drawing.Point(12, 72);
            this.Answer.Name = "Answer";
            this.Answer.Size = new System.Drawing.Size(56, 17);
            this.Answer.TabIndex = 9;
            this.Answer.Text = "Ответ :";
            // 
            // answe
            // 
            this.answe.AutoSize = true;
            this.answe.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.answe.Location = new System.Drawing.Point(91, 72);
            this.answe.Name = "answe";
            this.answe.Size = new System.Drawing.Size(0, 17);
            this.answe.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(244, 262);
            this.Controls.Add(this.answe);
            this.Controls.Add(this.Answer);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Sign);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.Multiply);
            this.Controls.Add(this.Minus);
            this.Controls.Add(this.Divide);
            this.Controls.Add(this.Equel);
            this.Controls.Add(this.Plus);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Plus;
        private System.Windows.Forms.Button Equel;
        private System.Windows.Forms.Button Divide;
        private System.Windows.Forms.Button Minus;
        private System.Windows.Forms.Button Multiply;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label Sign;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label Answer;
        private System.Windows.Forms.Label answe;
    }
}

