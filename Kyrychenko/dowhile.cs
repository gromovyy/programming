﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            int i = 0;
            int[] a = new int[10];
            do
            {
                s = Console.ReadLine();
                a[i] = Convert.ToInt32(s);
                i++;
            } while (i <= 9);
            Console.WriteLine("Stop!");

            i = 0;
            do
            {
                Console.WriteLine(Convert.ToString(a[i]));
                i++;
            } while (i <= 9);
            Console.ReadLine();
        }
    }
}